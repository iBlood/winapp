﻿using System;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;
using iBlood.Core.App;
using iBlood.Core.Class;

namespace iBlood
{
    public partial class Main : Form
    {
        private string comPort = "COM3";
        private int offset = 0;
        private SafeSerialPort serialComPort = null;
        private int prumernyTep = 0;

        public override sealed Size MinimumSize
        {
            get { return base.MinimumSize; }
            set { base.MinimumSize = value; }
        }

        public override sealed Size MaximumSize
        {
            get { return base.MaximumSize; }
            set { base.MaximumSize = value; }
        }


        public Main()
        {
            InitializeComponent();

            MinimumSize = Size;
            MaximumSize = Size;
            MaximizeBox = false;

        }


        // ReSharper disable once InconsistentNaming
        private void ConnectComPort(string _comPort, bool data = true)
        {
            serialComPort = new SafeSerialPort(_comPort, 115200, Parity.None, 8, StopBits.One);
            if (data)
            {
                serialComPort.DataReceived += SerialDataReceived;
            }
            serialComPort.Open();
        }


        /// <summary>
        /// Slouží pro příjem dat ze sériového portu
        /// </summary>
        private void SerialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadLine();
            if (indata.Substring(0, 1) == "B")
            {
                realTimeValue.Invoke((MethodInvoker)(() => VypisDat(indata.Substring(1))));
            }
        }

        private int cyklu = 0;
        private void VypisDat(string vystup)
        {
            realTimeValue.Text = vystup;
            int _vystup = Int32.Parse(vystup);
            if (_vystup < 200)
            {
                int min = Int32.Parse(minValue.Text);
                int max = Int32.Parse(maxValue.Text);
                prumernyTep += _vystup;

                if (min > _vystup || min == 0)
                {
                    minValue.Text = vystup;
                }

                if(max < _vystup)
                {
                    maxValue.Text = vystup;
                }


                cyklu++;
                label7.Text = cyklu.ToString();
            }
        }


        private void ToggleMeter_Click(object sender, EventArgs e)
        {
            if (serialComPort != null)
            {
                ToggleMeter.Text = "Start";
                label5.Text = "Nepřipojeno";
                serialComPort.Close();
                serialComPort = null;
            }
            else
            {
                ToggleMeter.Text = "Stop";
                label7.Text = "0";
                cyklu = 0;
                prumernyTep = 0;
                label5.Text = "Připojeno";
                ConnectComPort(comPort);
            }
        }

        private void MenuSetting_Click(object sender, EventArgs e)
        {
            var setting = new Core.App.Setting
            {
                StartPosition = FormStartPosition.CenterScreen
            };
            setting.ShowDialog();
        }

        private void ShowAboutDialog_Click(object sender, EventArgs e)
        {
            var about = new About
            {
                StartPosition = FormStartPosition.CenterScreen
            };
            about.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            realTimeValue.Text = "0";
            minValue.Text = "0";
            maxValue.Text = "0";
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (serialComPort == null)
            {
                int prumer = prumernyTep/Int32.Parse(label7.Text);
                ConnectComPort(comPort);
                serialComPort.Write(String.Format("print;{0};{1};{2};{3}", maxValue.Text, minValue.Text, label7.Text, prumer.ToString()));
                serialComPort.Close();
                serialComPort = null;
            }
        }
    }
}
