﻿using iBlood.Properties;

namespace iBlood
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopMenu = new System.Windows.Forms.ToolStrip();
            this.MenuSetting = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToggleMeter = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.ShowAboutDialog = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.InfoValuesLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.realTimeValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.maxValue = new System.Windows.Forms.Label();
            this.minValue = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TopMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopMenu
            // 
            this.TopMenu.BackColor = System.Drawing.Color.GhostWhite;
            this.TopMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.TopMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuSetting,
            this.toolStripSeparator1,
            this.ToggleMeter,
            this.toolStripButton2,
            this.toolStripButton3,
            this.ShowAboutDialog});
            this.TopMenu.Location = new System.Drawing.Point(0, 0);
            this.TopMenu.Name = "TopMenu";
            this.TopMenu.Size = new System.Drawing.Size(526, 25);
            this.TopMenu.TabIndex = 0;
            this.TopMenu.Text = "TopMenu";
            // 
            // MenuSetting
            // 
            this.MenuSetting.Image = global::iBlood.Properties.Resources.setting_tools;
            this.MenuSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MenuSetting.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.MenuSetting.Name = "MenuSetting";
            this.MenuSetting.Size = new System.Drawing.Size(79, 22);
            this.MenuSetting.Text = "Nastavení";
            this.MenuSetting.Click += new System.EventHandler(this.MenuSetting_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToggleMeter
            // 
            this.ToggleMeter.AutoToolTip = false;
            this.ToggleMeter.Image = global::iBlood.Properties.Resources.flag_flyaway_green;
            this.ToggleMeter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToggleMeter.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.ToggleMeter.Name = "ToggleMeter";
            this.ToggleMeter.Size = new System.Drawing.Size(51, 22);
            this.ToggleMeter.Text = "Start";
            this.ToggleMeter.ToolTipText = "Zapnutí nebo vypnutí celého měření";
            this.ToggleMeter.Click += new System.EventHandler(this.ToggleMeter_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::iBlood.Properties.Resources.filter_delete;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(65, 22);
            this.toolStripButton2.Text = "Vyčistit";
            this.toolStripButton2.ToolTipText = "Vyčistit naměřená data";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::iBlood.Properties.Resources.printer;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Margin = new System.Windows.Forms.Padding(5, 1, 0, 2);
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(48, 22);
            this.toolStripButton3.Text = "Tisk";
            this.toolStripButton3.ToolTipText = "Vytisknout naměřená data";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // ShowAboutDialog
            // 
            this.ShowAboutDialog.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ShowAboutDialog.Image = global::iBlood.Properties.Resources.emotion_face_panda;
            this.ShowAboutDialog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ShowAboutDialog.Name = "ShowAboutDialog";
            this.ShowAboutDialog.Size = new System.Drawing.Size(60, 22);
            this.ShowAboutDialog.Text = "Autoři";
            this.ShowAboutDialog.Click += new System.EventHandler(this.ShowAboutDialog_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.InfoValuesLabel);
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(0, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(523, 404);
            this.panel2.TabIndex = 1;
            // 
            // InfoValuesLabel
            // 
            this.InfoValuesLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.InfoValuesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.InfoValuesLabel.Location = new System.Drawing.Point(0, 0);
            this.InfoValuesLabel.Name = "InfoValuesLabel";
            this.InfoValuesLabel.Size = new System.Drawing.Size(523, 56);
            this.InfoValuesLabel.TabIndex = 1;
            this.InfoValuesLabel.Text = "Naměřené hodnoty";
            this.InfoValuesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.realTimeValue, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.maxValue, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.minValue, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 59);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(523, 345);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(265, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(253, 35);
            this.label5.TabIndex = 7;
            this.label5.Text = "Nepřipojeno";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(5, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(252, 35);
            this.label3.TabIndex = 6;
            this.label3.Text = "Stav";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(5, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Aktuální hodnota";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(5, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Maximální hodnota";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // realTimeValue
            // 
            this.realTimeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.realTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.realTimeValue.Location = new System.Drawing.Point(265, 2);
            this.realTimeValue.Name = "realTimeValue";
            this.realTimeValue.Size = new System.Drawing.Size(253, 35);
            this.realTimeValue.TabIndex = 2;
            this.realTimeValue.Text = "0";
            this.realTimeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(5, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(252, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Minimální hodnota";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // maxValue
            // 
            this.maxValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxValue.Location = new System.Drawing.Point(265, 76);
            this.maxValue.Name = "maxValue";
            this.maxValue.Size = new System.Drawing.Size(253, 25);
            this.maxValue.TabIndex = 4;
            this.maxValue.Text = "0";
            this.maxValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // minValue
            // 
            this.minValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minValue.Location = new System.Drawing.Point(265, 103);
            this.minValue.Name = "minValue";
            this.minValue.Size = new System.Drawing.Size(253, 25);
            this.minValue.TabIndex = 5;
            this.minValue.Text = "0";
            this.minValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(5, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(252, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Počet měřících cyklů";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(265, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(253, 25);
            this.label7.TabIndex = 9;
            this.label7.Text = "0";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 444);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.TopMenu);
            this.Icon = global::iBlood.Properties.Resources.AppIcon;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iBlood";
            this.TopMenu.ResumeLayout(false);
            this.TopMenu.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip TopMenu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToggleMeter;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton MenuSetting;
        private System.Windows.Forms.ToolStripButton ShowAboutDialog;
        private System.Windows.Forms.Label InfoValuesLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label realTimeValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label maxValue;
        private System.Windows.Forms.Label minValue;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

