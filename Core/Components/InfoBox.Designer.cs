﻿namespace iBlood.Core.Components
{
    partial class InfoBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BoxText = new System.Windows.Forms.Label();
            this.BoxImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // BoxText
            // 
            this.BoxText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BoxText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BoxText.Location = new System.Drawing.Point(38, 0);
            this.BoxText.Name = "BoxText";
            this.BoxText.Size = new System.Drawing.Size(273, 32);
            this.BoxText.TabIndex = 0;
            this.BoxText.Text = "Zde bude nějaká pěkná informace";
            this.BoxText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BoxImage
            // 
            this.BoxImage.Location = new System.Drawing.Point(0, 0);
            this.BoxImage.Name = "BoxImage";
            this.BoxImage.Size = new System.Drawing.Size(32, 32);
            this.BoxImage.TabIndex = 1;
            this.BoxImage.TabStop = false;
            // 
            // InfoBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BoxImage);
            this.Controls.Add(this.BoxText);
            this.Name = "InfoBox";
            this.Size = new System.Drawing.Size(311, 32);
            ((System.ComponentModel.ISupportInitialize)(this.BoxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label BoxText;
        private System.Windows.Forms.PictureBox BoxImage;
    }
}
