﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iBlood.Core.Components
{
    public partial class InfoBox : UserControl
    {

        [Category("Nastavení")]
        [Description("Obrázek boxu")]
        // ReSharper disable once ConvertToAutoProperty
        public Image PictureBox
        {
            get { return BoxImage.Image; }
            set { BoxImage.Image = value; }
        }


        [Category("Nastavení")]
        [Description("Text boxu")]
        // ReSharper disable once ConvertToAutoProperty
        public string TextBox
        {
            get { return BoxText.Text; }
            set { BoxText.Text = value; }
        }

        public InfoBox()
        {
            InitializeComponent();
        }
    }
}
