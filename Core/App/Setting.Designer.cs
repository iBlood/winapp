﻿namespace iBlood.Core.App
{
    partial class Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setting));
            this.SettingMenu = new System.Windows.Forms.TabControl();
            this.tabConnect = new System.Windows.Forms.TabPage();
            this.LayoutConnect = new System.Windows.Forms.TableLayoutPanel();
            this.LabelTypConnection = new System.Windows.Forms.Label();
            this.LabelSelectPort = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Parity = new System.Windows.Forms.ComboBox();
            this.ParityInfo = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.BaudRate = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.ComPort = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.TypConnection = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Protokol = new System.Windows.Forms.ComboBox();
            this.ProtokolInfo = new System.Windows.Forms.Label();
            this.TabList = new System.Windows.Forms.ImageList(this.components);
            this.SaveSetting = new System.Windows.Forms.Button();
            this.Controllist = new System.Windows.Forms.ImageList(this.components);
            this.BoxConnect = new iBlood.Core.Components.InfoBox();
            this.SettingMenu.SuspendLayout();
            this.tabConnect.SuspendLayout();
            this.LayoutConnect.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SettingMenu
            // 
            this.SettingMenu.Controls.Add(this.tabConnect);
            this.SettingMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.SettingMenu.ImageList = this.TabList;
            this.SettingMenu.Location = new System.Drawing.Point(0, 0);
            this.SettingMenu.Name = "SettingMenu";
            this.SettingMenu.SelectedIndex = 0;
            this.SettingMenu.Size = new System.Drawing.Size(543, 319);
            this.SettingMenu.TabIndex = 0;
            // 
            // tabConnect
            // 
            this.tabConnect.Controls.Add(this.LayoutConnect);
            this.tabConnect.Controls.Add(this.BoxConnect);
            this.tabConnect.ImageKey = "network_wireless.png";
            this.tabConnect.Location = new System.Drawing.Point(4, 23);
            this.tabConnect.Name = "tabConnect";
            this.tabConnect.Size = new System.Drawing.Size(535, 292);
            this.tabConnect.TabIndex = 0;
            this.tabConnect.Text = "Připojení";
            this.tabConnect.ToolTipText = "Nastavení připojování zařízení";
            this.tabConnect.UseVisualStyleBackColor = true;
            // 
            // LayoutConnect
            // 
            this.LayoutConnect.ColumnCount = 3;
            this.LayoutConnect.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.28947F));
            this.LayoutConnect.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.71053F));
            this.LayoutConnect.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.LayoutConnect.Controls.Add(this.LabelTypConnection, 0, 0);
            this.LayoutConnect.Controls.Add(this.LabelSelectPort, 0, 1);
            this.LayoutConnect.Controls.Add(this.label3, 0, 2);
            this.LayoutConnect.Controls.Add(this.label4, 0, 3);
            this.LayoutConnect.Controls.Add(this.tableLayoutPanel2, 1, 3);
            this.LayoutConnect.Controls.Add(this.tableLayoutPanel3, 1, 2);
            this.LayoutConnect.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.LayoutConnect.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.LayoutConnect.Controls.Add(this.label1, 0, 4);
            this.LayoutConnect.Controls.Add(this.tableLayoutPanel1, 1, 4);
            this.LayoutConnect.Location = new System.Drawing.Point(8, 41);
            this.LayoutConnect.Name = "LayoutConnect";
            this.LayoutConnect.RowCount = 6;
            this.LayoutConnect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.LayoutConnect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.LayoutConnect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.LayoutConnect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.LayoutConnect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.LayoutConnect.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.LayoutConnect.Size = new System.Drawing.Size(524, 248);
            this.LayoutConnect.TabIndex = 2;
            // 
            // LabelTypConnection
            // 
            this.LabelTypConnection.AutoSize = true;
            this.LabelTypConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelTypConnection.Location = new System.Drawing.Point(3, 0);
            this.LabelTypConnection.Name = "LabelTypConnection";
            this.LabelTypConnection.Size = new System.Drawing.Size(119, 33);
            this.LabelTypConnection.TabIndex = 0;
            this.LabelTypConnection.Text = "Typ připojení:";
            this.LabelTypConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelSelectPort
            // 
            this.LabelSelectPort.AutoSize = true;
            this.LabelSelectPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelSelectPort.Location = new System.Drawing.Point(3, 33);
            this.LabelSelectPort.Name = "LabelSelectPort";
            this.LabelSelectPort.Size = new System.Drawing.Size(119, 33);
            this.LabelSelectPort.TabIndex = 1;
            this.LabelSelectPort.Text = "Vybrat port:";
            this.LabelSelectPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 33);
            this.label3.TabIndex = 2;
            this.label3.Text = "Modulační rychlost:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 33);
            this.label4.TabIndex = 3;
            this.label4.Text = "Parita:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.tableLayoutPanel2.Controls.Add(this.Parity, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ParityInfo, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(128, 102);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(310, 27);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // Parity
            // 
            this.Parity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Parity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Parity.FormattingEnabled = true;
            this.Parity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Mark",
            "Odd",
            "Space"});
            this.Parity.Location = new System.Drawing.Point(3, 3);
            this.Parity.Name = "Parity";
            this.Parity.Size = new System.Drawing.Size(97, 21);
            this.Parity.TabIndex = 9;
            this.Parity.SelectedIndexChanged += new System.EventHandler(this.Parity_SelectedIndexChanged);
            // 
            // ParityInfo
            // 
            this.ParityInfo.AutoSize = true;
            this.ParityInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParityInfo.Location = new System.Drawing.Point(106, 0);
            this.ParityInfo.Name = "ParityInfo";
            this.ParityInfo.Size = new System.Drawing.Size(201, 27);
            this.ParityInfo.TabIndex = 10;
            this.ParityInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.BaudRate, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(128, 69);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(310, 27);
            this.tableLayoutPanel3.TabIndex = 14;
            // 
            // BaudRate
            // 
            this.BaudRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BaudRate.FormattingEnabled = true;
            this.BaudRate.Items.AddRange(new object[] {
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "115200",
            "230400"});
            this.BaudRate.Location = new System.Drawing.Point(3, 3);
            this.BaudRate.Name = "BaudRate";
            this.BaudRate.Size = new System.Drawing.Size(304, 21);
            this.BaudRate.TabIndex = 8;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.ComPort, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(128, 36);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(310, 27);
            this.tableLayoutPanel4.TabIndex = 15;
            // 
            // ComPort
            // 
            this.ComPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComPort.FormattingEnabled = true;
            this.ComPort.Location = new System.Drawing.Point(3, 3);
            this.ComPort.Name = "ComPort";
            this.ComPort.Size = new System.Drawing.Size(304, 21);
            this.ComPort.TabIndex = 7;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.TypConnection, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(128, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(310, 27);
            this.tableLayoutPanel5.TabIndex = 16;
            // 
            // TypConnection
            // 
            this.TypConnection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TypConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TypConnection.Location = new System.Drawing.Point(3, 0);
            this.TypConnection.Name = "TypConnection";
            this.TypConnection.Size = new System.Drawing.Size(304, 27);
            this.TypConnection.TabIndex = 6;
            this.TypConnection.Text = "USB, COM port";
            this.TypConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 33);
            this.label1.TabIndex = 17;
            this.label1.Text = "Protokol:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.tableLayoutPanel1.Controls.Add(this.Protokol, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ProtokolInfo, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(128, 135);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(310, 27);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // Protokol
            // 
            this.Protokol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Protokol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Protokol.FormattingEnabled = true;
            this.Protokol.Items.AddRange(new object[] {
            "iBlood v1",
            "iBlood v2"});
            this.Protokol.Location = new System.Drawing.Point(3, 3);
            this.Protokol.Name = "Protokol";
            this.Protokol.Size = new System.Drawing.Size(97, 21);
            this.Protokol.TabIndex = 0;
            this.Protokol.SelectedIndexChanged += new System.EventHandler(this.Protokol_SelectedIndexChanged);
            // 
            // ProtokolInfo
            // 
            this.ProtokolInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProtokolInfo.Location = new System.Drawing.Point(106, 0);
            this.ProtokolInfo.Name = "ProtokolInfo";
            this.ProtokolInfo.Size = new System.Drawing.Size(201, 27);
            this.ProtokolInfo.TabIndex = 1;
            this.ProtokolInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TabList
            // 
            this.TabList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TabList.ImageStream")));
            this.TabList.TransparentColor = System.Drawing.Color.Transparent;
            this.TabList.Images.SetKeyName(0, "network_wireless.png");
            this.TabList.Images.SetKeyName(1, "arrow_refresh.png");
            this.TabList.Images.SetKeyName(2, "database_save.png");
            this.TabList.Images.SetKeyName(3, "processor.png");
            // 
            // SaveSetting
            // 
            this.SaveSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SaveSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveSetting.ImageKey = "database_save.png";
            this.SaveSetting.ImageList = this.TabList;
            this.SaveSetting.Location = new System.Drawing.Point(4, 321);
            this.SaveSetting.Name = "SaveSetting";
            this.SaveSetting.Size = new System.Drawing.Size(131, 30);
            this.SaveSetting.TabIndex = 1;
            this.SaveSetting.Text = "Uložit nastavení";
            this.SaveSetting.UseVisualStyleBackColor = true;
            this.SaveSetting.Click += new System.EventHandler(this.SaveSetting_Click);
            // 
            // Controllist
            // 
            this.Controllist.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Controllist.ImageStream")));
            this.Controllist.TransparentColor = System.Drawing.Color.Transparent;
            this.Controllist.Images.SetKeyName(0, "tick.png");
            this.Controllist.Images.SetKeyName(1, "error.png");
            // 
            // BoxConnect
            // 
            this.BoxConnect.Location = new System.Drawing.Point(8, 3);
            this.BoxConnect.Name = "BoxConnect";
            this.BoxConnect.PictureBox = global::iBlood.Properties.Resources.help;
            this.BoxConnect.Size = new System.Drawing.Size(519, 32);
            this.BoxConnect.TabIndex = 1;
            this.BoxConnect.TextBox = "Nastavení připojení programu se zařízením";
            // 
            // Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 363);
            this.Controls.Add(this.SaveSetting);
            this.Controls.Add(this.SettingMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = global::iBlood.Properties.Resources.AppIcon;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Setting";
            this.Text = "Nastavení aplikace";
            this.SettingMenu.ResumeLayout(false);
            this.tabConnect.ResumeLayout(false);
            this.LayoutConnect.ResumeLayout(false);
            this.LayoutConnect.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl SettingMenu;
        private System.Windows.Forms.TabPage tabConnect;
        private System.Windows.Forms.ImageList TabList;
        private System.Windows.Forms.Button SaveSetting;
        private Components.InfoBox BoxConnect;
        private System.Windows.Forms.TableLayoutPanel LayoutConnect;
        private System.Windows.Forms.Label LabelTypConnection;
        private System.Windows.Forms.Label LabelSelectPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox Parity;
        private System.Windows.Forms.Label ParityInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox BaudRate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.ComboBox ComPort;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label TypConnection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox Protokol;
        private System.Windows.Forms.Label ProtokolInfo;
        private System.Windows.Forms.ImageList Controllist;
    }
}