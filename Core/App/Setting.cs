﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace iBlood.Core.App
{
    public partial class Setting : Form
    {
        public Setting()
        {
            InitializeComponent();
        }

        private void Parity_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selected = Parity.GetItemText(Parity.SelectedItem);
            var selectDictionary = new Dictionary<string, string>
            {
                {"None", "Vypnuta kontrola parity"},
                {"Even", "Nastavuje paritní bit tak aby počet byl sudé číslo"},
                {"Mark", "Ponechává paritní bit na hodnotě 1"},
                {"Odd", "Nastavuje paritní bit tak aby počet byl liché číslo"},
                {"Space", "Ponechává paritní bit na hodnotě 0"}
            };
            ParityInfo.Text = selectDictionary[selected];
        }

        private void Protokol_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selected = Protokol.GetItemText(Protokol.SelectedItem);
            var selectDictionary = new Dictionary<string, string>
            {
                {"iBlood v1", "Nejstarší komunikační protokol - neumožňuje tisk (simplex)"},
                {"iBlood v2", "Nejnovější protokol umožňující přenos větších dat - umožňuje i tisk (half-duplex)"}
            };
            ProtokolInfo.Text = selectDictionary[selected];
        }


        private void SaveAsFile_Click(object sender, EventArgs e)
        {

        }

        private void RecoveryFromFile_Click(object sender, EventArgs e)
        {
            
        }

        private void SaveSetting_Click(object sender, EventArgs e)
        {

        }
    }
}
