﻿using System.IO.Ports;

namespace iBlood.Core.Class
{
    class SafeSerialPort : SerialPort
    {
        public SafeSerialPort(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
        : base(portName, baudRate, parity, dataBits, stopBits)
        {

        }
    }
}
