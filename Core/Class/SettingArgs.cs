﻿namespace iBlood.Core.Class
{
    public class SettingArgs
    {
        private readonly string _comPort;
        private readonly int _offset;

        public SettingArgs(string comPort, int offset)
        {
            _comPort = comPort;
            _offset = offset;
        }

        public string GetComPort()
        {
            return _comPort;
        }

        public int GetOffset()
        {
            return _offset;
        }
    }
}
