﻿using System;

namespace iBlood.Core.Protocol.Exceptions
{
    /// <summary>
    ///     Špatná verifikace protokolu
    /// </summary>
    internal class BadVerifyException : Exception
    {
        public BadVerifyException()
        {
        }

        public BadVerifyException(string message)
            : base(message)
        {
        }

        public BadVerifyException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}