﻿namespace iBlood.Core.Protocol
{
    interface IProtocol
    {
        /// <summary>
        /// Ověření validních dat
        /// </summary>
        /// <param name="input">Vstupní text ze sériové komunikace</param>
        /// <returns>Vrácení zda-li jsou validní data</returns>
        bool verify(string input);

        /// <summary>
        /// Vrácení dat pomocí paměťového objektu
        /// </summary>
        /// <param name="input">Vstupní text ze sériové komunikace</param>
        /// <returns>Vrácení paměťového objektu</returns>
        ProtocolMemory parser(string input);
    }
}
