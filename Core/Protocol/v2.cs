﻿using System;
using System.Text.RegularExpressions;
using System.Xml.Schema;
using iBlood.Core.Protocol.Exceptions;

namespace iBlood.Core.Protocol
{
    [Serializable]
    class v2 : IProtocol
    {
        private const string Pattern = @"\$IB([0-9]{1})([A-Z]{1})\;([A-Za-z0-9]+)";

        public ProtocolMemory parser(string input)
        {
            if (!verify(input)){ throw new BadVerifyException("[BadVerify_v2] " + input); }
            var regex = new Regex(Pattern, RegexOptions.IgnoreCase);
            var match = regex.Match(input);

            var dataType = byte.Parse(match.Groups[0].Value);
            var protocolVersion = match.Groups[0].Value[0];
            var protocolData = match.Groups[0].Value;

            return new ProtocolMemory(dataType, protocolVersion, protocolData);
        }

        public bool verify(string input)
        {
            var regex = new Regex(Pattern, RegexOptions.IgnoreCase);
            return regex.IsMatch(input);
        }
    }
}
