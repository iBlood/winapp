﻿using System;
using System.CodeDom;
using System.Runtime.InteropServices.WindowsRuntime;

namespace iBlood.Core.Protocol
{
    class ProtocolMemory
    {
        /// <summary>
        /// Verze protokolu
        /// </summary>
        private byte version;

        /// <summary>
        /// Typ protokolu
        /// </summary>
        private char type;

        /// <summary>
        /// Data protokolu
        /// </summary>
        private string data;

        /// <summary>
        ///  Nastavení dat protokolu
        /// </summary>
        /// <param name="version">Verze</param>
        /// <param name="type">Typ</param>
        /// <param name="data">Data</param>
        public ProtocolMemory(byte version, char type, string data)
        {
            setVersion(version);
            setType(type);
            setData(data);
        }

        /// <summary>
        /// Nastavení verze protokolu
        /// </summary>
        /// <param name="_version">Protokol</param>
        public void setVersion(byte _version)
        {
            version = _version;
        }

        /// <summary>
        /// Nastavení typu protokolu
        /// </summary>
        /// <param name="_type">Typ protokolu</param>
        public void setType(char _type)
        {
            type = _type;
        }

        /// <summary>
        /// Nastavení dat protokolu
        /// </summary>
        /// <param name="_data">Data protokolu</param>
        public void setData(string _data)
        {
            data = _data;
        }

        /// <summary>
        /// Vybrání verze protokolu
        /// </summary>
        /// <returns>Verze protokolu</returns>
        public byte getVersion()
        {
            return version;
        }

        /// <summary>
        /// Typ protokolu
        /// </summary>
        /// <returns>Typ protokolu</returns>
        public char getType()
        {
            return type;
        }

        /// <summary>
        /// Data protokolu
        /// </summary>
        /// <returns>Data</returns>
        public string getData()
        {
            return data;
        }
    }
}
