﻿using System;
using iBlood.Core.Class;

namespace iBlood.Core.Events
{
    public delegate void MySaveSetting(object source, SaveSettingArgs e);

    public class SaveSettingArgs : EventArgs
    {
        private readonly SettingArgs _args;

        public SaveSettingArgs(SettingArgs args)
        {
            _args = args;
        }


        public SettingArgs GetArgs()
        {
            return _args;
        }
    }
}
